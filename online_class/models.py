from django.db import models
from django.utils import timezone
# Create your models here.

class OnlineClass(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=250, unique=True)
    description = models.TextField(max_length=250)
    class_key = models.CharField(max_length=25, unique=True)
    start_time = models.DateTimeField(null=False)
    end_time = models.DateTimeField(null=False)
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(default=timezone.now)

    class Meta:
        db_table =  'tbl_online_class'

class Learner(models.Model):

    id = models.AutoField(primary_key=True)
    class_id = models.ForeignKey(OnlineClass, related_name="belong_class", null = False, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    email = models.EmailField(max_length=250, unique=True)
    password = models.CharField(max_length=250)
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(default=timezone.now)

    class Meta:
        db_table =  'tbl_leaner'

class LearnerEmotion(models.Model):

    id = models.AutoField(primary_key=True)
    learner_id = models.ForeignKey(Learner, related_name="emotion_of_learner", null = False, on_delete=models.CASCADE)
    class_id = models.ForeignKey(OnlineClass, related_name="learn_in_class", null = False, on_delete=models.CASCADE)
    neutral = models.IntegerField(default=0)
    happiness = models.IntegerField(default=0)
    surprise = models.IntegerField(default=0)
    sadness = models.IntegerField(default=0)
    anger = models.IntegerField(default=0)
    disgust = models.IntegerField(default=0)
    fear = models.IntegerField(default=0)
    contempt = models.IntegerField(default=0)
    date = models.DateField()
    hour = models.IntegerField()
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(default=timezone.now)

    class Meta:
        db_table =  'tbl_emotion_leaner'