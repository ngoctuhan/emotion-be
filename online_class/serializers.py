from rest_framework import serializers
from .models import *

class OnlineClassSerializer(serializers.ModelSerializer):

    class Meta:
        model = OnlineClass
        fields = '__all__'


class LearnerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Learner
        fields = '__all__'
   
class LearnerEmotionSerializer(serializers.ModelSerializer):

    class Meta:
        model = LearnerEmotion 
        fields = '__all__'
