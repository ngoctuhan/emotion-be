from django.urls import path
from online_class import views

app_name = 'online'

urlpatterns = [
    path('class/home', views.online_class, name = "class"),
    path('class/home/<int:id>', views.show_detail_room, name= "show-detail-room"),
    path('class/<int:id>', views.manage_student, name = "class-student"),
    path('class', views.ClassAPI.as_view(), name = "manage-online-class"),
    path('leaner', views.LearnerAPI.as_view(), name = "manage-learner"),
    path('leaner/<int:pk>', views.LearnerRUD.as_view(), name = "manage-learner-specific-pk"),
    path('event/emotion', views.EventEmotionAPI.as_view(), name = "manage-learner"),
    path('learner/activity', views.LearnerAccessAPI.as_view(), name = "learner-access"),
    path('class/<int:pk>', views.ClassRUD.as_view(), name = "class-cud"),

]