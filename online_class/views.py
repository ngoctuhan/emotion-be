# Create your views here.
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.translation import gettext as _
from rest_framework import generics, status, permissions
from .models import *
from .serializers import *
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime
from datetime import datetime
from django.db.models import Sum
from manager.permissions import ApiKeyAuth
@login_required
def online_class(request):

    list_online_class = OnlineClass.objects.all()
    return render(request, 'online_class/class_main.html', context={"classes": list_online_class})

@login_required
def manage_student(request, id:int):
    list_student = Learner.objects.filter(class_id=id)
    return render(request, 'online_class/learner_main.html', context={"id": id,"learners": list_student})

@login_required
def show_detail_room(request, id: id):

    # count number of student and quality of a room
    students = Learner.objects.filter(class_id=id)
    num_students = students.count()
    estimate = estimate_quality(id)
    return render(request, 'online_class/class_detail.html', 
                  context={"id": id, "quality": estimate, "num_students": num_students, "students": students})



def estimate_quality(class_id):
    queryset = LearnerEmotion.objects.all()
    queryset = queryset.filter(class_id=class_id).aggregate(
        neutral = Sum('neutral'),
        happiness=Sum('happiness'),
        surprise=Sum('surprise'),
        sadness=Sum('sadness'),
        anger=Sum('anger'),
        disgust=Sum('disgust'),
        fear=Sum('fear'),
        contempt=Sum('contempt')
    )
    emotion_data = {k: 0 if v is None else v for k, v in queryset.items()}
    total_emotions = max(sum(emotion_data.values()), 1e-7)
    probabilities = {key: value / total_emotions for key, value in emotion_data.items()}
    
    # Define weights
    weights = {
        'neutral': 0.8,
        'happiness': 0.9,
        'surprise': 0.85,
        'sadness': 0.7,
        'anger': 0.4,
        'disgust': 0.2,
        'fear': 0.25,
        'contempt': 0.3
    }

    # Calculate weighted score
    weighted_score = sum(probabilities[key] * weights[key] for key in emotion_data.keys())

    # Determine the emotional status based on the weighted score
    if weighted_score >= 0.85:
        emotional_status = "Rất Hứng thú"
    elif 0.8 <= weighted_score < 0.85:
        emotional_status = "Hứng thú"
    elif 0.75 <= weighted_score < 0.8:
        emotional_status = "Tập Trung"
    elif 0.7 <= weighted_score < 0.75:
        emotional_status = "Mất tập trung"
    else:
        emotional_status = "Báo Động"
    return emotional_status

class LearnerAccessAPI(APIView):
    
    def post(self,  request, *args, **kwargs):
        
        payload = request.data 
        class_key = payload['class_key']
        email = payload['email']
        password = payload['password']
        leaner = Learner.objects.filter(email=email, password=password)
        if not leaner:
            return Response({
                    "status": False, 
                    "message": "Email or password is incorrect"
                },  status=status.HTTP_401_UNAUTHORIZED)
        else:
            current_time = datetime.now()
            room = OnlineClass.objects.filter(class_key=class_key, 
                                              start_time__lt=current_time, 
                                              end_time__gt=current_time)
            if not room:
                return Response({
                    "status": False, 
                    "message": "Class is not available"
                },  status=status.HTTP_404_NOT_FOUND)
        return Response({
            "status": True,
            "message": "You can join class now",
            "data": {
                "leaner_id": leaner[0].id,
                "class_id": room[0].id 
            }
        }, status=status.HTTP_200_OK)
                
class LearnerAPI(generics.ListCreateAPIView):

    queryset = Learner.objects.all()
    serializer_class = LearnerSerializer 
    permission_classes = [permissions.IsAuthenticated]

class LearnerRUD(generics.RetrieveUpdateDestroyAPIView):
    queryset = Learner.objects.all()
    serializer_class = LearnerSerializer 
    permission_classes = [permissions.IsAuthenticated]

class ClassRUD(generics.RetrieveUpdateDestroyAPIView):
    queryset = OnlineClass.objects.all()
    serializer_class = OnlineClassSerializer
    permission_classes = [permissions.IsAuthenticated]

class ClassAPI(generics.ListCreateAPIView):

    queryset = OnlineClass.objects.all()
    serializer_class = OnlineClassSerializer
    permission_classes = [permissions.IsAuthenticated]

class EventEmotionAPI(generics.ListCreateAPIView):

    queryset = LearnerEmotion.objects.all()
    serializer_class = LearnerEmotionSerializer
    # permission_classes = [permissions.IsAuthenticated]
    
    def get(self, request, *args, **kwargs):

        queryset = LearnerEmotion.objects.all()
        student_id = self.request.query_params.get('studentId', None)
        class_id = self.request.query_params.get('classId', None)

        if student_id is not None:
            queryset = LearnerEmotion.objects.filter(learner_id=student_id).aggregate(
                neutral = Sum('neutral'),
                happiness=Sum('happiness'),
                surprise=Sum('surprise'),
                sadness=Sum('sadness'),
                anger=Sum('anger'),
                disgust=Sum('disgust'),
                fear=Sum('fear'),
                contempt=Sum('contempt')
            )
        
        elif class_id is not None:
            queryset = queryset.filter(class_id=class_id).aggregate(
                neutral = Sum('neutral'),
                happiness=Sum('happiness'),
                surprise=Sum('surprise'),
                sadness=Sum('sadness'),
                anger=Sum('anger'),
                disgust=Sum('disgust'),
                fear=Sum('fear'),
                contempt=Sum('contempt')
            )
        return Response(queryset)

    def post(self, request, *args, **kwargs):
       
        data = request.data
        learner_id = data.get('learner_id')
        class_id = data.get('class_id')
        date = data.get('date')
        hour = data.get('hour')
        try:
            emotion_instance = LearnerEmotion.objects.get(learner_id=learner_id, class_id=class_id, date=date, hour=hour)
            # Update the existing record
            emotion_instance.neutral += int(data.get('neutral', 0))
            emotion_instance.happiness += int(data.get('happiness', 0))
            emotion_instance.surprise += int(data.get('surprise', 0))
            emotion_instance.sadness += int(data.get('sadness', 0))
            emotion_instance.anger += int(data.get('anger', 0))
            emotion_instance.disgust += int(data.get('disgust', 0))
            emotion_instance.fear += int(data.get('fear', 0))
            emotion_instance.contempt += int(data.get('contempt', 0))
            emotion_instance.updated = timezone.now()
            emotion_instance.save()
            serializer = LearnerEmotionSerializer(emotion_instance)
            return Response(serializer.data, status=status.HTTP_200_OK)

        except LearnerEmotion.DoesNotExist:
            # Create a new record
            serializer = LearnerEmotionSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class EmotionOnlineDetail(APIView):
    
    permission_classes = (ApiKeyAuth,)
   
    def post(self, request, *args, **kwargs):

        data = request.data 
        mac_address = data['class_key']
        onlineclass = OnlineClass.objects.filter(class_key=mac_address)
        if len(onlineclass) == 0:
            return Response({
                "status": False, 
                "data": {},
                "image": []
            })
        start_time = data['start_time']
        end_time = data['end_time']
        start_time = datetime.strptime(start_time, "%Y-%m-%d %H:%M:%S.%f")
        end_time = datetime.strptime(end_time, "%Y-%m-%d %H:%M:%S.%f")
        # ['neutral', 'happiness', 'surprise', 'sadness', 'anger', 'disgust', 'fear', 'contempt']
        emotion_data =  LearnerEmotion.objects.filter(class_id=onlineclass[0].id ,created__range=(start_time, end_time)).aggregate(
            neutral = Sum('neutral'),
            happiness=Sum('happiness'),
            surprise=Sum('surprise'),
            sadness=Sum('sadness'),
            anger=Sum('anger'),
            disgust=Sum('disgust'),
            fear=Sum('fear'),
            contempt=Sum('contempt')
        )
        emotion_data = {k: 0 if v is None else v for k, v in emotion_data.items()}
        # Calculate total occurrences of emotions to derive probabilities
        total_emotions = sum(emotion_data.values())
        if total_emotions == 0:
            return Response({"status": False, "message": "No emotion data available."}, status=status.HTTP_404_NOT_FOUND)

        probabilities = {key: value / total_emotions for key, value in emotion_data.items()}

        # Define weights
        weights = {
            'neutral': 0.8,
            'happiness': 0.9,
            'surprise': 0.85,
            'sadness': 0.7,
            'anger': 0.4,
            'disgust': 0.2,
            'fear': 0.25,
            'contempt': 0.3
        }

        # Calculate weighted score
        weighted_score = sum(probabilities[key] * weights[key] for key in emotion_data.keys())

        # Determine the emotional status based on the weighted score
        if weighted_score >= 0.85:
            emotional_status = "Rất Hứng thú"
        elif 0.8 <= weighted_score < 0.85:
            emotional_status = "Hứng thú"
        elif 0.75 <= weighted_score < 0.8:
            emotional_status = "Tập Trung"
        elif 0.7 <= weighted_score < 0.75:
            emotional_status = "Mất tập trung"
        else:
            emotional_status = "Báo Động"

        return Response({
            "status": True, 
            "data": emotion_data,
            "emotional_status": emotional_status,
            "weighted_score": weighted_score,
            "image": []
        })