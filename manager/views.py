from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.translation import gettext as _
from rest_framework.decorators import api_view
from django.shortcuts import get_object_or_404
from rest_framework import generics, status, permissions
from manager.models import *
from manager.serializers import *
from datetime import datetime
from rest_framework.response import Response
from django.core.files.storage import FileSystemStorage
from rest_framework.views import APIView
import os
from django.http import JsonResponse, HttpResponse
from django.core import serializers
from django.db.models import Q
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from .mqtt_helper import MQTT_Connection
from datetime import date, datetime
from django.db.models import Q
from datetime import datetime
from django.db.models import Sum
from .permissions import ApiKeyAuth
from rest_framework.decorators import api_view

@login_required
def classview(request):
    return render(request, "manager/manager_class.html", {}) 

@login_required
def detail_cluster(request, id:int):
    cluster = Cluster.objects.filter(id=id)[0]
    list_camera = Configure.objects.filter(clusterID=cluster.id)
    return render(request, "manager/class_detail.html", context={"cluster": cluster, "camera": list_camera}) 

@login_required
@api_view(['POST'])
def upload_file(request):

    data = request.FILES.get('file')
    if data is not None:
        file_save = data
        fss = FileSystemStorage()
        filename =  fss.save(file_save.name, file_save)
        uploaded_file_url = fss.url(filename)
        return Response({"status": True, "url": uploaded_file_url }, status=status.HTTP_201_CREATED)
    else:
        return Response({"status" : False}, status = status.HTTP_406_NOT_ACCEPTABLE)

class ClusterList(generics.ListCreateAPIView):
    
    queryset = Cluster.objects.all()
    serializer_class = ClusterSerializer 
    permission_classes = [permissions.IsAuthenticated]

class ClusterDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Cluster.objects.all()
    serializer_class = ClusterSerializer 
    permission_classes = [permissions.IsAuthenticated]

#----------------------------------------------------------
class ConfigureList(generics.ListCreateAPIView):
    
    queryset = Configure.objects.all()
    serializer_class = ConfigureSerializer 
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, *args, **kwargs):
        data = request.data 
        cluster = data['clusterID']
        old_config = Configure.objects.filter(clusterID=int(cluster))
        # remove all old config
        if len(old_config) > 0:
            old_config.delete()
        save_records = []
        for i, url in enumerate(data['urls']):
            if url == "":
                url = "rtsp://"
            new_record = {"camera_url": url, "clusterID": int(cluster)}
            save_records.append(new_record)

        serializer = self.get_serializer(data=save_records, many=True)
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

class ConfigureDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Configure.objects.all()
    serializer_class = ConfigureSerializer 
    permission_classes = [permissions.IsAuthenticated]

class EmotionDetail(APIView):
    
    permission_classes = (ApiKeyAuth,)
    def patch(self, request, *args, **kwargs):
        """
        Sample: {"clusterID":1, "neutral" : 10,  "happiness" : 15,  "surprise" : 5,  "sadness" : 7,  "anger" : 10,  "disgust" : 2,  "fear" : 0,  "contempt": 30}
        """
        data = request.data 
        clusterId = data['clusterID'] 
        today = date.today() 
        hour = datetime.now().hour
        current_record = Emotion.objects.filter(clusterID=clusterId, date=today, hour=hour)
       
        if len(current_record) == 0:
            # first update so need create new record
            emotion_data = data 
            emotion_data['clusterID'] = clusterId
            emotion_data['date'] = today 
            emotion_data['hour'] = hour 
            serializer = EmotionSerializer(data=emotion_data)

        else:
            emotion = EmotionSerializer(instance=current_record[0])
            data_update = {}
            labels = ['neutral', 'happiness', 'surprise', 'sadness', 'anger', 'disgust', 'fear', 'contempt']
            for label in labels:
                data_update[label] = emotion.data[label] + int(data[label])
            data_update['clusterID'] = emotion.data['clusterID'] 
            data_update['date'] = emotion.data['date'] 
            data_update['hour'] = emotion.data['hour']
            serializer = EmotionSerializer(current_record[0], data=data_update)
           
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer.save()
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)(
                f'progress_{clusterId}', 
                {"type": "text_message", "text": serializer.data}
            )    
            return Response(serializer.data, status=status.HTTP_200_OK)
    
    def post(self, request, *args, **kwargs):

        data = request.data 
        mac_address = data['mac_address']
        clusterId = Cluster.objects.filter(mac_address=mac_address)
        if len(clusterId) == 0:
            return Response({
                "status": False, 
                "data": {},
                "image": []
            })
        start_time = data['start_time']
        end_time = data['end_time']
        start_time = datetime.strptime(start_time, "%Y-%m-%d %H:%M:%S.%f")
        end_time = datetime.strptime(end_time, "%Y-%m-%d %H:%M:%S.%f")
        # ['neutral', 'happiness', 'surprise', 'sadness', 'anger', 'disgust', 'fear', 'contempt']
        emotion_data =  Emotion.objects.filter(clusterID=clusterId[0].id ,created__range=(start_time, end_time)).aggregate(
            neutral = Sum('neutral'),
            happiness=Sum('happiness'),
            surprise=Sum('surprise'),
            sadness=Sum('sadness'),
            anger=Sum('anger'),
            disgust=Sum('disgust'),
            fear=Sum('fear'),
            contempt=Sum('contempt')
        )
        emotion_data = {k: 0 if v is None else v for k, v in emotion_data.items()}
        # Calculate total occurrences of emotions to derive probabilities
        total_emotions = sum(emotion_data.values())
        if total_emotions == 0:
            return Response({"status": False, "message": "No emotion data available."}, status=status.HTTP_404_NOT_FOUND)

        probabilities = {key: value / total_emotions for key, value in emotion_data.items()}

        # Define weights
        weights = {
            'neutral': 0.8,
            'happiness': 0.9,
            'surprise': 0.85,
            'sadness': 0.7,
            'anger': 0.4,
            'disgust': 0.2,
            'fear': 0.25,
            'contempt': 0.3
        }

        # Calculate weighted score
        weighted_score = sum(probabilities[key] * weights[key] for key in emotion_data.keys())

        # Determine the emotional status based on the weighted score
        if weighted_score >= 0.85:
            emotional_status = "Rất Hứng thú"
        elif 0.8 <= weighted_score < 0.85:
            emotional_status = "Hứng thú"
        elif 0.75 <= weighted_score < 0.8:
            emotional_status = "Tập Trung"
        elif 0.7 <= weighted_score < 0.75:
            emotional_status = "Mất tập trung"
        else:
            emotional_status = "Báo Động"

        return Response({
            "status": True, 
            "data": emotion_data,
            "emotional_status": emotional_status,
            "weighted_score": weighted_score,
            "image": []
        })

class ClusterData(APIView):
    
    def get(self, request, *args, **kwargs):
        clusterId = request.GET['id'] 
        info = request.GET['info']
        if info == "camera":
            try:
                # convert mac to camera
                cluster = Cluster.objects.filter(mac_address=clusterId)
                if len(cluster) > 0:
                    clusterId = cluster[0].id
                configs = Configure.objects.filter(clusterID=int(clusterId))
                data_serializer = ConfigureSerializer(data=configs, many=True)
                data_serializer.is_valid()
                return Response({
                    "status": True, 
                    "clusterId": int(clusterId),
                    "data": data_serializer.data
                },  status=status.HTTP_200_OK)
            except:
                return Response({
                    "status": False, 
                    "clusterId": clusterId,
                    "data": [],
                },  status=status.HTTP_404_NOT_FOUND)
          
        else:
            data = Emotion.objects.filter(clusterID=clusterId).order_by('updated').latest('updated')
            data_serializer = EmotionSerializer(data=[data], many=True)
            data_serializer.is_valid()
            return Response({
                    "status": True, 
                    "data": data_serializer.data
                },  status=status.HTTP_200_OK)

    def post(self,  request, *args, **kwargs):
        data = request.data 
        data_cluster = {"name": data['name'], "description" : len(data['ip_camera']), "mac_address": data['mac_address'] }
        cluster_serializer = ClusterSerializer(data=data_cluster)
        if not cluster_serializer.is_valid():
            return Response(
                cluster_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            cluster_serializer.save()
            old_ip_camera = Configure.objects.filter(clusterID=cluster_serializer.data['id'])
            if len(old_ip_camera) > 0:
                old_ip_camera.delete()
            data_saved = []
            for ip in data['ip_camera']:
                new_ip = {"camera_url": ip, "clusterID": cluster_serializer.data['id']}
                data_saved.append(new_ip)
            config_serializer = ConfigureSerializer(data=data_saved, many=True)
            if not config_serializer.is_valid():
                return Response(
                    cluster_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            else:
                config_serializer.save()
                return Response({
                    "status": True, 
                    "cluster": cluster_serializer.data,
                    "camera": config_serializer.data,
                },  status=status.HTTP_200_OK)