from rest_framework import serializers
from .models import *

class ClusterSerializer(serializers.ModelSerializer):

    class Meta:
        model = Cluster
        fields = ['id', 'name', 'mac_address', 'description', 'created', 'updated', 'status']


class ConfigureSerializer(serializers.ModelSerializer):

    class Meta:
        model = Configure
        fields = ['id', 'camera_url', 'clusterID']
   
class EmotionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Emotion 
        fields = ['id', 'clusterID', 'neutral', 'happiness', 'surprise', 
                'sadness', 'anger', 'disgust', 'fear', 'contempt',
                'image_capture', 'date', 'hour' ,'created', 'updated'
            ]

