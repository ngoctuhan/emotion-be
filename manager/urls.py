from django.urls import path
from manager import views

app_name = 'manager'

urlpatterns = [
    path('class', views.classview, name = "class"),
    path('class/info/<int:id>', views.detail_cluster, name = "detail-cluster"),
    path('classes', views.ClusterList.as_view(), name = "classes-list"),
    path('class/<int:pk>', views.ClusterDetail.as_view(), name = "classes-detail"),
    path('configures', views.ConfigureList.as_view(), name = "configure-list"),
    path('configure/<int:pk>', views.ConfigureDetail.as_view(), name = "configure-detail"),
    path('cluster/info', views.ClusterData.as_view(), name = "cluster-information"),
    path('emotion', views.EmotionDetail.as_view(), name = "emotion-information"),
]