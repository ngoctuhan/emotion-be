from django.db import models
from django.utils import timezone
# Create your models here.

class Cluster(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=250, unique=True)
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(default=timezone.now)
    description =  models.CharField(max_length=250)
    status = models.BooleanField(default=False)
    mac_address = models.CharField(max_length=30)
    
    class Meta:
        db_table =  'tbl_cluster'

class Configure(models.Model):

    id = models.AutoField(primary_key=True)
    clusterID = models.ForeignKey(Cluster, related_name="config_of_cluster", null = False, on_delete=models.CASCADE)
    camera_url = models.CharField(max_length=250)
    class Meta:
        db_table = 'tbl_configure'

class Emotion(models.Model):

    id = models.AutoField(primary_key=True)
    clusterID = models.ForeignKey(Cluster, related_name="emotion_of_cluster", null = False, on_delete=models.CASCADE)
    neutral = models.IntegerField(default=0)
    happiness = models.IntegerField(default=0)
    surprise = models.IntegerField(default=0)
    sadness = models.IntegerField(default=0)
    anger = models.IntegerField(default=0)
    disgust = models.IntegerField(default=0)
    fear = models.IntegerField(default=0)
    contempt = models.IntegerField(default=0)
    image_capture = models.CharField(max_length=250, blank = True)
    date = models.DateField()
    hour = models.IntegerField()
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(default=timezone.now)
    
    class Meta:
        db_table = 'tbl_emotion'
