# Emotion manager
 
Project will provide a code to make web portal for configure and monitor device tracking emotion in classroom

## Components

- Projects needs

+ MQTT Broken 
+ MySQL is major database
+ Redis for caching 

## Installation

Recommend with docker 


```bash
docker-compose up -d --build 
```

Default port of API is: 12004


## Usage 

http://127.0.0.1:12004 

You should create supper admin before

To create superadmin 

Access inside container 

```
docker exec -it <id of container> bash
```

Run and follow instruction at terminal

```
python3 manage.py createsuperuser 
```
