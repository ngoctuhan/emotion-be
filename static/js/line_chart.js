var lineChart = null;
var maxColumns3 = 45;  // Represent from minute 1 to minute 45
var count1 = -1;
var check = 0;
var globalData = [];
var previousData = [];

var weights = {
    'key1': 0.8,
    'key2': 0.9,
    'key3': 0.85,
    'key4': 0.7,
    'key5': 0.4,
    'key6': 0.2,
    'key7': 0.25,
    'key8': 0.3
};

function get_default_format_data3() {
    const xLabels = Array.from({ length: maxColumns3 }, (_, i) => `${i + 1}m`);
    const dataObj = {
        labels: xLabels,
        datasets: [{
            label: 'Score',
            data: Array(maxColumns3).fill(0),
            borderColor: '#3498db', // Blue color
            borderWidth: 2,
            fill: false
        }]
    };

    return dataObj;
}

function update_line_chart(data) {
    check++;
    console.log("check:", check);
    console.log("count1:", count1);
    if (count1 < maxColumns3) {
        if (count1 === -1) {  // First time, just save the data and skip
            previousData = [...data];
            console.log("previousData:",  previousData);
            count1++;
        } else {
            if(check % 10 === 0) {
                let totalNew = data.reduce((acc, val) => acc + val, 0);
                let totalOld = previousData.reduce((acc, val) => acc + val, 0);
                let differences = data.map((value, index) => value - previousData[index]);
                let probabilities = differences.map(diff => diff / (totalNew - totalOld));
                let scores = probabilities.map((probability, index) => probability * weights[`key${index+1}`]);
                let score = scores.reduce((acc, val) => acc + val, 0);

                lineChart.data.datasets[0].data[count1] = score;
                lineChart.update();

                previousData = [...data];
                count1++;
            }
        }
    } else {
        check = 0;
        count1 = -1;  // Reset count after 45 minutes
        previousData = [];
        lineChart.data.datasets[0].data.fill(0);
        lineChart.update();
    }
}
function init_line_chart() {
    const data = get_default_format_data3();
    const config = {
        type: "line",
        data,
        options: {
            scales: {
                y: {
                    position: 'left',
                    beginAtZero: true,
                    title: {
                        display: true,
                        text: 'Emotion'
                    },
                    grid: {
                        display: true
                    },
                    ticks: {
                        min: 0.5,
                        max: 1,
                        stepSize: 0.05,
                        callback: function(value, index, values) {
                            if (value === 0.85) {
                                return 'Rất Hứng thú';
                            } else if (value >= 0.8 && value < 0.85) {
                                return 'Hứng thú';
                            } else if (value >= 0.75 && value < 0.8) {
                                return 'Tập trung';
                            } else if (value >= 0.7 && value < 0.75) {
                                return 'Mất tập trung';
                            } else {
                                return '';
                            }
                        }
                    }
                },
               
                x: {
                    display: true,
                    title: {
                        display: true,
                        text: 'Time (minutes)'
                    },
                    grid: {
                        display: false
                    }
                }
            },
            plugins: {
                title: {
                    display: true,
                    text: "Total Emotions Time"
                }
            },
            elements: {
                line: {
                    spanGaps: true
                },
                point: {
                    radius: function(context) {
                        var index = context.dataIndex;
                        var value = context.dataset.data[index];
                        return value < 0.8 ? 5 : 3;
                    },
                    backgroundColor: function(context) {
                        var index = context.dataIndex;
                        var value = context.dataset.data[index];
                        return value < 0.8 ? "red" : "#3498db";
                    }
                }
            }
        }
    };

    lineChart = new Chart(document.getElementById("line-chart"), config);

    blinkPoints();
}

function blinkPoints() {
    let count = 0;
    const intervalId = setInterval(() => {
        if (lineChart && count < 5000) {
            lineChart.data.datasets[0].pointBackgroundColor = lineChart.data.datasets[0].data.map((value, index) => {
                if (value < 0.8 && index >= count) {
                    return count % 2 === 0 ? "red" : "transparent";
                } else {
                    return value < 0.8 ? (count % 2 === 0 ? "red" : "transparent") : "#3498db";
                }
            });
            lineChart.update();
            count++;
        } else {
            clearInterval(intervalId);
        }
    }, 500); // Sử dụng thời gian 500ms giữa các sự thay đổi trạng thái (đèn tắt/sáng)
}



