var stackChart2 = null;
var maxColumns2 = 6;
var count=-1;
var check=true;
var globalData = [];

function covert_ISO_to_date2(ISO_date_str) {
    let date = new Date(ISO_date_str);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let dt = date.getDate();
    if (dt < 10) {
        dt = "0" + dt;
    }
    if (month < 10) {
        month = "0" + month;
    }
    return year + "-" + month + "-" + dt;
}

function get_default_format_data2(labels, data) {
    const xLabels = Array.from({ length: maxColumns2 }, (_, i) => ((i + 1) * 5).toString() + "s");

    const dataObj = {
        labels: xLabels,
        datasets: labels.map((label, index) => ({
            label: label,
            data: Array(maxColumns2).fill(0),  // Khởi tạo dữ liệu ban đầu là 0
            backgroundColor: [
                "#3498db",
                "#e67e22",
                
                "#2ecc71",
                "#f1c40f",
                "#e74c3c",
                "#9b59b6",
                "#34495e",
                "#95a5a6",
            ][index],
            borderWidth: 1,
        })),
    };

    return dataObj;
}

function get_default_chart2() {
    const labels = [
        "neutral",
        "happiness",
        "surprise",
        "sadness",
        "anger",
        "disgust",
        "fear",
        "contempt",
    ];

    // Tạo mảng dữ liệu mẫu, ban đầu tất cả giá trị là 0
    const data = Array(maxColumns2).fill(0);

    // Sử dụng hàm get_default_format_data1 để tạo dữ liệu ban đầu
    return get_default_format_data2(labels, data);
}

function update_stack_chart2(data) {
    if(count==-1){
      globalData = [...data];
      count++;

    }
    else{


    if (count < maxColumns2) {
        
        stackChart2.data.datasets.forEach((dataset, index) => {
            dataset.data[count] = count === 0 ? data[index]-globalData[index]: dataset.data[count - 1] + data[index]-globalData[index];
        });
        count++;
        globalData = [...data];
    } else {
        count=-1;
        stackChart2.data.datasets.forEach((dataset, index) => {
            dataset.data.fill(0);
           
        });
    }
   
}

   
    stackChart2.update();
}

function init_stack_chart2() {
    const data = get_default_chart2();
    const config = {
        type: "bar",
        data,
        options: {
            scales: {
                x: {
                    stacked: true,
                },
                y: {
                    stacked: true,
                },
            },
            plugins: {
                title: {
                    display: true,
                    text: "Tổng cảm xúc theo thời gian",
                },
            },
        },
    };
    stackChart2 = new Chart(document.getElementById("stack-chart2"), config);

    setInterval(function () {
        // Gọi hàm lấy dữ liệu mới và cập nhật biểu đồ (get_info_chart cần được định nghĩa)
        get_info_chart();
    }, 5000);
}

// Hãy nhớ định nghĩa hàm get_info_chart() để lấy dữ liệu mới và gọi update_stack_chart()