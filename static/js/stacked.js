var stackChart = null;
var maxColumns = 6;
var allZeros = Array(8).fill(0);
var check1=true;

function covert_ISO_to_date1(ISO_date_str) {
    let date = new Date(ISO_date_str);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let dt = date.getDate();
    if (dt < 10) {
        dt = "0" + dt;
    }
    if (month < 10) {
        month = "0" + month;
    }
    return year + "-" + month + "-" + dt;
}

function get_default_format_data1(labels, data) {
    const xLabels = Array.from({ length: maxColumns2 }, (_, i) => ((i + 1) * 5).toString() + "s");

    const dataObj = {
        labels: xLabels,
        datasets: labels.map((label, index) => ({
            label: label,
            data: Array(maxColumns2).fill(0),  // Khởi tạo dữ liệu ban đầu là 0
            backgroundColor: [
                "#3498db",
                "#e67e22",
                
                "#2ecc71",
                "#f1c40f",
                "#e74c3c",
                "#9b59b6",
                "#34495e",
                "#95a5a6",
            ][index],
            borderWidth: 1,
        })),
    };

    return dataObj;
}
function get_default_chart1() {
    labels = [
        "neutral",
        "happiness",
        "surprise",
        "sadness",
        "anger",
        "disgust",
        "fear",
        "contempt",
    ];

    // Tạo một mảng dữ liệu mẫu, tất cả giá trị đều là 0 ban đầu
    const data = Array(maxColumns).fill(0);

    // Sử dụng hàm get_default_format_data1 để tạo dữ liệu ban đầu
    return get_default_format_data1(labels, data);
}

function update_stack_chart(labels, data) {
    

    // Lấy giá trị mới từ dữ liệu đầu vào
    console.log("Giá trị của count:", data);
   
    const newValues =  [...data];
   
    if(check1)
    {
        check1=false
    }
    else{

    // Duyệt qua từng dataset trong biểu đồ
    stackChart.data.datasets.forEach((dataset, index) => {
        // Di chuyển dữ liệu cũ sang phải
        for (let i = maxColumns - 1; i > 0; i--) {
            dataset.data[i] = dataset.data[i - 1];
        }

        // Cập nhật giá trị mới cho cột đầu tiên
        dataset.data[0] = newValues[index] - allZeros[index];
        console.log(`Dataset ${index} - Cột đầu tiên: ${dataset.data[0]}`);
        
    });
}

    // Cập nhật biểu đồ sau khi đã cập nhật dữ liệu
    stackChart.update();
    allZeros=[...data];
    

}

function init_stack_chart() {
    // Khởi tạo biểu đồ
    data = get_default_chart1();
    const config = {
        type: "bar",
        data,
        options: {
            scales: {
                x: {
                    stacked: true,
                },
                y: {
                    stacked: true,
                },
            },
            plugins: {
                title: {
                    display: true,
                    text: "Số lượng cảm xúc theo thời gian",
                },
            },
        },
    };
    stackChart = new Chart(document.getElementById("stack-chart"), config);

    // Gọi hàm cập nhật tự động mỗi 5 giây
    setInterval(function () {
        // Gọi hàm lấy dữ liệu mới và cập nhật biểu đồ
        get_info_chart();
    }, 5000);
}
